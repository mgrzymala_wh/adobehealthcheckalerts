USE [DMOperations]
GO

DROP FUNCTION IF EXISTS dbo.ufn_CreateHTMLCSS
GO

CREATE FUNCTION dbo.ufn_CreateHTMLCSS
(
    @ColumnWidth NVARCHAR(32)
)
RETURNS XML
AS
BEGIN

RETURN 
(
    SELECT 
	'<head>
	<style type="text/css">
    table {
	font:12pt tahoma,arial,sans-serif;
    width: '+@ColumnWidth+';
	}
	caption {
	color:#FFFF00;
	font:bold 12pt tahoma,arial,sans-serif;
	background-color:#204c7d;
	border:1px solid #DCDCDC;
	border-collapse:collapse;
	padding-left:5px;
	padding-right:5px;
	}
	th {
	color:#FFFFFF;
	font:bold 12pt tahoma,arial,sans-serif;
	background-color:#204c7d;
	border:1px solid #DCDCDC;
	border-collapse:collapse;
	padding-left:5px;
	padding-right:5px;
	}
	td {
	color:#000000;
	font:10pt tahoma,arial,sans-serif;
	border:1px solid #DCDCDC;
	border-collapse:collapse;
	padding-left:3px;
	padding-right:3px;
	}
	.Warning {
	background-color:#FFFF00; 
	color:#2E2E2E;
    text-align: right;
    font-weight: bold;
	}
	.Critical {
	background-color:#FF0000;
	color:#FFFFFF;
    text-align: right;
    font-weight: bold;
	}
	.Healthy {
	background-color:#458B00;
	color:#FFFFFF;
    text-align: right;
    font-weight: bold;
	}
	h1 {
	color:#FFFFFF;
	font:bold 16pt arial,sans-serif;
	background-color:#204c7d;
	text-align:center;
	}
	h2 {
	color:#204c7d;
	font:bold 14pt arial,sans-serif;
	}
	h3 {
	color:#204c7d;
	font:bold 12pt arial,sans-serif;
	}
    h4 {
	color:#FFFFFF;
	font:bold 14pt arial,sans-serif;
	background-color:#204c7d;
	text-align:center;
    width: '+@ColumnWidth+';
	}
	body {
	color:#000000;
	font:12pt tahoma,arial,sans-serif;
	margin:0px;
	padding:0px;
	}
	</style>
	</head>'
) 
END
GO