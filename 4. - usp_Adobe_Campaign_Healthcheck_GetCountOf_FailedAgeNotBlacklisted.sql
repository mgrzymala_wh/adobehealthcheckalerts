USE [DMOperations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        WHGROUP\dskowronski
-- Create date:   30/10/2020
-- Description:   DANSDPSRE-221 - Create usp_Adobe_Campaign_Healthcheck_GetCountOf_FailedAgeNotBlacklisted
--                Suppression Rule 1 count of all customer accounts that have failed age
--                verification and are not blacklisted. If this count fails it means UNO BSL
--                contains customers who have failed AV but have not been blacklisted and must
--                be investigated immediately.

-- List of abbreviations:
-- cadp = BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile
-- cafp = BusinessSemanticLayer.dbo.CustomerAccountFlagProfile
-- =============================================
-- =============================================
-- Change History
-- Date                 User                     Change                
-- 30/10/2020           dskowronski              Created DANSDPSRE-221
-- =============================================

CREATE OR ALTER PROCEDURE dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_FailedAgeNotBlacklisted]
AS
BEGIN
    DECLARE @Adobe_Campaign_CountOf_FailedAgeNotBlacklisted INT;
    BEGIN TRY

        SELECT
            @Adobe_Campaign_CountOf_FailedAgeNotBlacklisted = COUNT(1)
        FROM
            BusinessSemanticLayer.dbo.CustomerAccount ca (NOLOCK) 
            INNER JOIN
                BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile cadp (NOLOCK) 
                ON ca.customeraccountid = cadp.CustomerAccountId 
            INNER JOIN
                BusinessSemanticLayer.dbo.CustomerAccountFlagProfile cafp (NOLOCK) 
                ON ca.CustomerAccountId = cafp.CustomerAccountId 
        WHERE
            (
                cafp.UkAgeVerificationFlag = 'N' AND cafp.NonUkAgeVerificationFlag = 'N'
            )
            AND ca.CustomerAccountId > 3 AND cadp.blacklistflag = 'N'

        RETURN COALESCE(@Adobe_Campaign_CountOf_FailedAgeNotBlacklisted, -1);
    END TRY
    
    BEGIN CATCH
        DECLARE 
             @@MESSAGE      VARCHAR(2048)
            ,@@PROCNAME     VARCHAR(255) 
            ,@@USERNAME     VARCHAR(32)
    
            ,@ErrorNumber   INT 
            ,@ErrorSeverity INT 
            ,@ErrorState    INT 
            ,@ErrorLine     INT 
            ,@ErrorMessage  VARCHAR(1000) 
     
        SELECT   
               @ErrorNumber  = ERROR_NUMBER() 
              ,@ErrorSeverity= ERROR_SEVERITY() 
              ,@ErrorState   = ERROR_STATE() 
              ,@ErrorLine    = ERROR_LINE() 
              ,@ErrorMessage = COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
    
        SELECT  @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')  
        SELECT  @@USERNAME = USER_NAME();  
        SELECT  @@MESSAGE = 'Procedure '       + @@PROCNAME 
                          + ' executed by '    + @@USERNAME + ' failed with error number: ' +CAST(@ErrorNumber AS VARCHAR)
                          +', severity: '      + CAST(@ErrorSeverity AS VARCHAR) 
                          +', state: '         + CAST(@ErrorState AS VARCHAR)
                          +', on line: '       + CAST(@ErrorLine AS VARCHAR)
                          +', with message: '  + @ErrorMessage;    
        RAISERROR (@@MESSAGE 
                  ,@ErrorSeverity 
                   -- Optionally override the error to cause a failure 
                   --,18 
                  ,@ErrorState
                  );
        RETURN -1
    END CATCH
END
GO
