USE [DMOperations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        WHGROUP\dskowronski
-- Create date:   30/10/2020
-- Description:   DANSDPSRE-221 - Create usp_Adobe_Campaign_Healthcheck_GetCountOf_FlagSetNotBlacklisted
--                Suppression Rule 5 count of all customer accounts that have a flag set that
--                should mean they are blacklisted but are not blacklisted. If this count
--                fails it means UNO BSL contains customers who have certain flags set that
--                should mean that they are blacklisted but they have not been blacklisted 
--                and must be investigated immediately.

-- List of abbreviations:
-- cadp = BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile
-- cafp = BusinessSemanticLayer.dbo.CustomerAccountFlagProfile
-- cas = BusinessSemanticLayer.dbo.CustomerAccountStatus

-- =============================================
-- =============================================
-- Change History
-- Date                 User                     Change                
-- 30/10/2020           dskowronski              Created DANSDPSRE-221
-- =============================================

DROP PROCEDURE IF EXISTS dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_FlagSetNotBlacklisted]
GO

CREATE OR ALTER PROCEDURE dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_FlagSetNotBlacklisted]
AS
BEGIN
    DECLARE @Adobe_Campaign_GetCountOf_FlagSetNotBlacklisted INT;
    BEGIN TRY

      SELECT
          @Adobe_Campaign_GetCountOf_FlagSetNotBlacklisted = COUNT(DISTINCT ca.customeraccountid)
      FROM
          BusinessSemanticLayer.dbo.CustomerAccount ca (nolock) 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile cadp (nolock) 
              ON ca.customeraccountid = cadp.CustomerAccountId 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountFlagProfile cafp (nolock) 
              ON ca.CustomerAccountId = cafp.CustomerAccountId 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountProfile cap (nolock) 
              ON ca.CustomerAccountId = cap.CustomerAccountId 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountStatus cas (nolock) 
              ON cap.CustomerAccountStatusId = cas.CustomerAccountStatusId 
          INNER JOIN
              BusinessSemanticLayer.dbo.Channel ch (nolock) 
              ON ch.ChannelId = ca.registrationchannelid 
      WHERE
          (
                 cafp.StopFlag = 'Y' 
              OR cafp.FraudFlag = 'Y' 
              OR cafp.PendingFlag = 'Y' 
              OR cafp.DebtStatus1Flag = 'Y' 
              OR cafp.DebtStatus2Flag = 'Y' 
              OR cafp.DebtStatus3Flag = 'Y' 
              OR cafp.DebtStatus4Flag = 'Y' 
              OR cafp.DebtStatus5Flag = 'Y' 
              OR cafp.DebtStatusStopFlag = 'Y' 
              OR cafp.SogeiMismatchFlag = 'Y' 
              OR cafp.CnjMigrFlag = 'Y' 
              OR cafp.CnjProhibFlag = 'Y' 
          )
          AND cadp.BlacklistFlag = 'N' AND ca.CustomerAccountId > 3

        RETURN COALESCE(@Adobe_Campaign_GetCountOf_FlagSetNotBlacklisted, -1);
    END TRY
    
    BEGIN CATCH
        DECLARE 
             @@MESSAGE      VARCHAR(2048)
            ,@@PROCNAME     VARCHAR(255) 
            ,@@USERNAME     VARCHAR(32)
    
            ,@ErrorNumber   INT 
            ,@ErrorSeverity INT 
            ,@ErrorState    INT 
            ,@ErrorLine     INT 
            ,@ErrorMessage  VARCHAR(1000) 
     
        SELECT   
               @ErrorNumber  = ERROR_NUMBER() 
              ,@ErrorSeverity= ERROR_SEVERITY() 
              ,@ErrorState   = ERROR_STATE() 
              ,@ErrorLine    = ERROR_LINE() 
              ,@ErrorMessage = COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
    
        SELECT  @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')  
        SELECT  @@USERNAME = USER_NAME();  
        SELECT  @@MESSAGE = 'Procedure '       + @@PROCNAME 
                          + ' executed by '    + @@USERNAME + ' failed with error number: ' +CAST(@ErrorNumber AS VARCHAR)
                          +', severity: '      + CAST(@ErrorSeverity AS VARCHAR) 
                          +', state: '         + CAST(@ErrorState AS VARCHAR)
                          +', on line: '       + CAST(@ErrorLine AS VARCHAR)
                          +', with message: '  + @ErrorMessage;    
        RAISERROR (@@MESSAGE 
                  ,@ErrorSeverity 
                   -- Optionally override the error to cause a failure 
                   --,18 
                  ,@ErrorState
                  );
        RETURN -1
    END CATCH
END
GO
