USE [DMOperations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        WHGROUP\dskowronski
-- Create date:   30/10/2020
-- Description:   DANSDPSRE-221 - Create usp_Adobe_Campaign_Healthcheck_GetCountOf_FlaggedAsTestAccountNotBlacklisted
--                Suppression Rule 4 count of all customer accounts that are flagged as test account and
--                are not blacklisted. If this count fails it means UNO BSL contains customers who have
--                been flagged as test accounts but have not been blacklisted and must be investigated 
--                immediately.

-- List of abbreviations:
-- cadp = BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile
-- cafp = BusinessSemanticLayer.dbo.CustomerAccountFlagProfile
-- cas = BusinessSemanticLayer.dbo.CustomerAccountStatus
-- =============================================
-- =============================================
-- Change History
-- Date                 User                     Change                
-- 30/10/2020           dskowronski              Created DANSDPSRE-221
-- =============================================

DROP PROCEDURE IF EXISTS [dbo].[usp_Adobe_Campaign_Healthcheck_GetCountOf_FlaggedAsTestAccountNotBlacklisted]
GO

CREATE OR ALTER PROCEDURE dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_FlaggedAsTestAccountNotBlacklisted]
AS
BEGIN
    DECLARE @Adobe_Campaign_GetCountOf_FlaggedAsTestAccountNotBlacklisted INT;
    BEGIN TRY

      SELECT
          @Adobe_Campaign_GetCountOf_FlaggedAsTestAccountNotBlacklisted = COUNT(1)
      FROM
          BusinessSemanticLayer.dbo.CustomerAccount ca (nolock) 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile cadp (nolock) 
              ON ca.CustomerAccountId = cadp.CustomerAccountId 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountFlagProfile cafp (nolock) 
              ON ca.CustomerAccountId = cafp.CustomerAccountId 
      WHERE
          cafp.TestAccountFlag = 'Y' AND cadp.blacklistflag = 'N' AND ca.CustomerAccountId > 3

      RETURN COALESCE(@Adobe_Campaign_GetCountOf_FlaggedAsTestAccountNotBlacklisted, -1);
    END TRY
    
    BEGIN CATCH
        DECLARE 
             @@MESSAGE      VARCHAR(2048)
            ,@@PROCNAME     VARCHAR(255) 
            ,@@USERNAME     VARCHAR(32)
    
            ,@ErrorNumber   INT 
            ,@ErrorSeverity INT 
            ,@ErrorState    INT 
            ,@ErrorLine     INT 
            ,@ErrorMessage  VARCHAR(1000) 
     
        SELECT   
               @ErrorNumber  = ERROR_NUMBER() 
              ,@ErrorSeverity= ERROR_SEVERITY() 
              ,@ErrorState   = ERROR_STATE() 
              ,@ErrorLine    = ERROR_LINE() 
              ,@ErrorMessage = COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
    
        SELECT  @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')  
        SELECT  @@USERNAME = USER_NAME();  
        SELECT  @@MESSAGE = 'Procedure '       + @@PROCNAME 
                          + ' executed by '    + @@USERNAME + ' failed with error number: ' +CAST(@ErrorNumber AS VARCHAR)
                          +', severity: '      + CAST(@ErrorSeverity AS VARCHAR) 
                          +', state: '         + CAST(@ErrorState AS VARCHAR)
                          +', on line: '       + CAST(@ErrorLine AS VARCHAR)
                          +', with message: '  + @ErrorMessage;    
        RAISERROR (@@MESSAGE 
                  ,@ErrorSeverity 
                   -- Optionally override the error to cause a failure 
                   --,18 
                  ,@ErrorState
                  );
        RETURN -1
    END CATCH
END
GO
