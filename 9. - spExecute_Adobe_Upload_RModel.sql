USE [DMOperations]
GO


SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
CREATE OR ALTER PROCEDURE [dbo].[spExecute_Adobe_Upload_RModel]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

		DECLARE @ErrMsg1		NVARCHAR(MAX)
			,@ErrMsg2			NVARCHAR(MAX)
			,@ErrMsg			NVARCHAR(MAX)
			,@ErrorProcedure	SYSNAME
			,@ErrorNumber		INT
			,@ErrorSeverity		INT
			,@ErrorState		INT
			,@ErrorLine			INT
			,@ErrorMessage		NVARCHAR(MAX)
			,@Status			INT
			,@ExecutionStatus	VARCHAR(50)
			,@Package_Name		VARCHAR(100)
			,@Enviroment_Variable	NVARCHAR(MAX)
			,@Compress		NVARCHAR(100)
			,@DestinationFolder		NVARCHAR(100)
			,@Encrypt				NVARCHAR(100)
			,@NumberOfRows			NVARCHAR(100)	
			,@PathToPublicKey		NVARCHAR(100)	
			,@TargetFile	NVARCHAR(100)	
			,@FileType NVARCHAR(10)
			,@ProcessedFolder NVARCHAR(100)
			,@Action				NVARCHAR(100)	
			,@CompletePath NVARCHAR(100)
			,@FileExtension	NVARCHAR(100)
			,@LocalDirectory		NVARCHAR(100)
			,@Password				NVARCHAR(100)
			,@PortNumber			NVARCHAR(100)	
			,@PrivateKey		NVARCHAR(100)	
			,@Proxy	NVARCHAR(100)	
			,@ServerDirectory				NVARCHAR(100)	
			,@ServerHostKey			NVARCHAR(100)	
			,@ServerName		NVARCHAR(100)	
			,@UserName	NVARCHAR(100)	
			,@WinSCPLocation				NVARCHAR(100)	
			,@OrchestrationVariable    AS OrchestrationVariable
			,@FrameworkOrchestrationErrorDsc	NVARCHAR(MAX)

	SET @ErrMsg2 = ''
	SET @Status = -1

	BEGIN TRY


			----EXECUTE ORCHESTRATION-----------------------------------------------------------------------------

					--Set package name 						
			SET @Package_Name = 'SFTPandFileSplit'

			-- Set archive directory from existing environment variable		
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_FileTask_Compress',
					@Package_Name, 
					@Compress OUTPUT

			-- Set archive directory from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_FileTask_DestinationFolder',
					@Package_Name, 
					@DestinationFolder OUTPUT

			-- Set max wait time from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_FileTask_Encrypt',
					@Package_Name, 
					@Encrypt OUTPUT

			-- Set source filename from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_FileTask_NumberOfRows',
					@Package_Name, 
					@NumberOfRows OUTPUT

			-- Set source file extension from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_FileTask_PathToPublicKey',
					@Package_Name, 
					@PathToPublicKey OUTPUT

			-- Set archive directory from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_FileTask_TargetFile',
					@Package_Name, 
					@TargetFile OUTPUT

			-- Set regex from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_FileTask_ProcessedFolder',
					@Package_Name, 
					@ProcessedFolder OUTPUT

			-- Set archive directory from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_FileTask_FileType',
					@Package_Name, 
					@FileType OUTPUT

			-- Set regex from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_Action',
					@Package_Name, 
					@Action OUTPUT

			-- Set archive directory from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_FileExtension',
					@Package_Name, 
					@FileExtension OUTPUT


			-- Set archive directory from existing environment variable			
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_LocalDirectory',
					@Package_Name, 
					@LocalDirectory OUTPUT
			

			-- Set max wait time from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_Password',
					@Package_Name, 
					@Password OUTPUT

			-- Set source filename from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_PortNumber',
					@Package_Name, 
					@PortNumber OUTPUT

			-- Set source filename from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_PrivateKey',
					@Package_Name, 
					@PrivateKey OUTPUT

			-- Set source file extension from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_Proxy',
					@Package_Name, 
					@Proxy OUTPUT

			-- Set regex from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_ServerDirectory',
					@Package_Name, 
					@ServerDirectory OUTPUT

			-- Set archive directory from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_ServerHostKey',
					@Package_Name, 
					@ServerHostKey OUTPUT

				-- Set source file extension from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_ServerName',
					@Package_Name, 
					@ServerName OUTPUT

			-- Set regex from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_UserName',
					@Package_Name, 
					@UserName OUTPUT

			-- Set archive directory from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_WinSCPLocation',
					@Package_Name, 
					@WinSCPLocation OUTPUT

			-- Set archive directory from existing environment variable	
			EXEC [dbo].[spGet_EnviromentVariable_Value] 
				    'PKP_SFTPTask_CompletePath',
					@Package_Name, 
					@CompletePath OUTPUT




			--Populate variables table with the relevant values
			INSERT INTO @OrchestrationVariable
			(
			VariableName
			,VariableValue
			,Sensitive
			)
			VALUES 
			 ( 'PKV_FileTask_Compress',							@Compress ,						0 )				 	
			,( 'PKV_FileTask_DestinationFolder',				@DestinationFolder,				0 )		
			,( 'PKV_FileTask_Encrypt',							@Encrypt,						0 )			 			 	 	
			,( 'PKV_FileTask_NumberOfRows',						@NumberOfRows,					0 )
			,( 'PKV_FileTask_PathToPublicKey',					@PathToPublicKey,				0 )
			,( 'PKV_FileTask_SourceFolder',						@TargetFile,					0 )
			,( 'PKV_FileTask_ProcessedFolder',					@ProcessedFolder,				0 )
			,( 'PKV_FileTask_FileType',							@FileType,						0 )
			,( 'PKV_SFTPTask_Action',							'Upload',						0 )
			,( 'PKV_SFTPTask_FileExtension',					@FileExtension,					0 )
			,( 'PKV_SFTPTask_LocalDirectory',					@LocalDirectory,				0 )
			,( 'PKV_SFTPTask_Password',							@Password ,						0 )				 	
			,( 'PKV_SFTPTask_PortNumber',						@PortNumber,					0 )		
			,( 'PKV_SFTPTask_PrivateKey',						@PrivateKey,					0 )			 			 	 	
			,( 'PKV_SFTPTask_Proxy',							@Proxy,							0 )
			,( 'PKV_SFTPTask_ServerDirectory',					@ServerDirectory,				0 )
			,( 'PKV_SFTPTask_ServerHostKey',					@ServerHostKey,					0 )
			,( 'PKV_SFTPTask_ServerName',						@ServerName,					0 )
			,( 'PKV_SFTPTask_UserName',							@UserName,						0 )
			,( 'PKV_SFTPTask_WinSCPLocation',					@WinSCPLocation,				0 )
			,( 'PKV_SFTPTask_CompletePath',						@CompletePath,					0 )

						
		--Execute package and capture the outcome				
		EXEC @Status = spExecutePackage @Package_Name = @Package_Name, @OrchestrationVariable = @OrchestrationVariable     


		--Raise error if the package didn't execute successfully 	
		IF @Status <> 7 AND @Status <> -1
		BEGIN

		SELECT @FrameworkOrchestrationErrorDsc = ISNULL(@Package_Name,N'Unknown')
											+ N'; failed with the following description: Max wait time exceeded.'

			RAISERROR (N'Framework Project: %s', -- Message text.
					   16, -- Severity,
					   127, -- State,
					   @FrameworkOrchestrationErrorDsc, -- First argument.
					   N''); -- Second argument.
		END
        	
	END TRY

	-- Trap Error if the proc fails
	BEGIN CATCH
		
		SELECT @ErrorProcedure	= ERROR_PROCEDURE()
				,@ErrorNumber	= ERROR_NUMBER()
				,@ErrorSeverity = ERROR_SEVERITY()
				,@ErrorState	= ERROR_STATE()
				,@ErrorLine		= ERROR_LINE()
				,@ErrorMessage	= ERROR_MESSAGE()

		-- Define Error Message and the GO TO spError
		-- Check Data Type of the Error Procedure to cater for extended stored procedures
		SELECT @ErrMsg1 = 'Error Procedure: ' + CASE SQL_VARIANT_PROPERTY(@ErrorProcedure,'BaseType' )
												WHEN 'varchar' THEN @ErrorProcedure
												ELSE 'unknown'
											END 
				+ ', Error Number: '	+ CAST(@ErrorNumber AS VARCHAR)
				+ ', Error Severity: '	+ CAST(@ErrorSeverity AS VARCHAR)
				+ ', Error State: '		+ CAST(@ErrorState AS VARCHAR)
				+ ', Error Line: '		+ CAST(@ErrorLine  AS VARCHAR)
				+ ', Error Message: '	+ @ErrorMessage
		
		/* Optional additional information
		SET @ErrMsg2 = 'Error executing:' + 
			'SELECT <' + @Param1 + ', sysname, '+ @p1 + '>, <' + @Param2 + ', sys-name, ' + @p2 + '>'
		*/
		GOTO spError

	END CATCH

	-- Return success
	RETURN @Status 
	
-- Return Failure
spERROR:

	-- Error Message
	SET @ErrMsg = 'Error executing: ' + OBJECT_NAME(@@PROCID) + CHAR(13) 
				+ ' - ' + ISNULL(@ErrMsg1, 'Unknown Error') + ' ' + CHAR(13) 
				/* 2008 Syntax
				+ CASE @ErrMsg2 
					WHEN ''
						THEN ''
						ELSE ' - Additional Information: ' + @ErrMsg2 + ' ' + CHAR(13)
					END
				*/
				-- 2012 syntax
				+ IIF(@ErrMsg2 ='', '', ' - Additional Information: ' + @ErrMsg2 + ' ' + CHAR(13))
				+ 'Server Name: ' + @@SERVERNAME + ' - Database: ' + DB_NAME() + ', User: ' +   SUSER_NAME() + ', SPID: ' + CAST(@@SPID AS VARCHAR) + ', Now: ' + CAST(GETDATE() AS VARCHAR(121));

	 -- Raise Error
	 RAISERROR (@ErrMsg
               ,@ErrorSeverity
			   -- Optionally override the error to cause a failure
			   --,18
               ,@ErrorState);

	RETURN @Status 
	
END
GO

