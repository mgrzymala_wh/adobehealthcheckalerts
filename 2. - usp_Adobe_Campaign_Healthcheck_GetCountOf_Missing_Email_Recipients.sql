USE [DMOperations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        WHGROUP\dskowronski
-- Create date:   29/10/2020
-- Description:   DANSDPSRE-221 - Create usp_Adobe_Campaign_Healthcheck_GetCountOf_Missing_Email_Recipients
--                Returns distinct count of RECIPIENT_IDs from [StagingAdobeInbound].[dbo].[recipients] with BSL.[CustomerAccountDerivedProfile].[BlacklistEmailFlag] = 'N'  

-- List of abbreviations:

-- bsl_cadp = [BusinessSemanticLayer].[dbo].[CustomerAccountDerivedProfile]
-- pl_cab   = [PresentationLayer].[dbo].[CustomerAccountBase]
-- st_uh    = [StagingAdobeInbound].[dbo].[Unsubscribe_History] 
-- st_r     = [StagingAdobeInbound].[dbo].[recipients]

-- =============================================
-- =============================================
-- Change History
-- Date                 User                     Change                
-- 29/10/2020           dskowronski              Created DANSDPSRE-221
-- 05/11/2020           mgrzymala                Moved 'DISTINCT' from SELECT DISTINCT ... to SELECT COUNT(DISTINCT
-- =============================================
DROP PROCEDURE IF EXISTS [usp_Adobe_Campaign_Healthcheck_GetCountOf_Missing_Email_Recipients]
GO

CREATE OR ALTER PROCEDURE dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_Missing_Email_Recipients]
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
    SET XACT_ABORT ON;
    
    DECLARE @EmailMissingRecipientCount INT;
    
    BEGIN TRY
        SELECT 
            @EmailMissingRecipientCount = COUNT(DISTINCT st_r.[RECIPIENT_ID]) 
        FROM
            [StagingAdobeInbound].[dbo].[recipients] AS st_r 
            INNER JOIN
                (
                    SELECT
                        [GLOBAL_ACCOUNT_ID],
                        MAX([RequestId]) req 
                    FROM
                        [StagingAdobeInbound].[dbo].[recipients] 
                    GROUP BY
                        [GLOBAL_ACCOUNT_ID]
                ) AS b 
                ON st_r.GLOBAL_ACCOUNT_ID = b.GLOBAL_ACCOUNT_ID 
                AND st_r.RequestId = b.req 
            INNER JOIN
                [BusinessSemanticLayer].[dbo].[CustomerAccountDerivedProfile] AS bsl_cadp 
                ON bsl_cadp.CustomerAccountId = st_r.[GLOBAL_ACCOUNT_ID] 
            INNER JOIN
                [PresentationLayer].[dbo].[CustomerAccountBase] AS pl_cab 
                ON bsl_cadp.[CustomerAccountId] = pl_cab.[CustomerAccountId] 
            INNER JOIN
                (
                    SELECT
                        st_uh.[RECIPIENT_ID],
                        pl_cab.[CustomerAccountId],
                        [RECIPIENT_EMAIL_ADDRESS],
                        [SOURCE_URL],
                        [CREATED] AS [CreatedDt],
                        [Message] 
                    FROM
                        [StagingAdobeInbound].[dbo].[Unsubscribe_History] AS st_uh 
                        INNER JOIN
                            (
                                SELECT DISTINCT
                                    RECIPIENT_ID,
                                    GLOBAL_ACCOUNT_ID 
                                FROM
                                    [StagingAdobeInbound].[dbo].[recipients]
                            ) AS r 
                            ON st_uh.RECIPIENT_ID = r.RECIPIENT_ID 
                        INNER JOIN
                            [PresentationLayer].[dbo].[CustomerAccountBase] AS pl_cab 
                            ON r.GLOBAL_ACCOUNT_ID = pl_cab.CustomerAccountId 
                        INNER JOIN
                            (
                                SELECT
                                    cb.[CustomerAccountId],
                                    MAX(st_uh.[RequestId]) reqid 
                                FROM
                                    [StagingAdobeInbound].[dbo].[Unsubscribe_History] AS st_uh 
                                    INNER JOIN
                                        [StagingAdobeInbound].[dbo].[recipients] AS r 
                                        ON st_uh.RECIPIENT_ID = r.RECIPIENT_ID 
                                    INNER JOIN
                                        [PresentationLayer].[dbo].[CustomerAccountBase] AS cb 
                                        ON r.GLOBAL_ACCOUNT_ID = cb.CustomerAccountId 
                                WHERE
                                    st_uh.[SOURCE_URL] LIKE '%unsub%' 
                                GROUP BY
                                    cb.[CustomerAccountId]
                            ) AS d 
                            ON st_uh.RequestId = d.reqid 
                            AND pl_cab.CustomerAccountId = d.CustomerAccountId 
                    WHERE
                            st_uh.[SOURCE_URL] LIKE '%unsub%' 
                    AND     st_uh.[CREATED] > '2020-09-13' 
                ) AS i 
                ON bsl_cadp.CustomerAccountId = i.CustomerAccountId 
        WHERE   1 = 1 
            AND bsl_cadp.[BlacklistFlag] <> 'Y' 
            AND bsl_cadp.[BlacklistEmailFlag] = 'N' 
            AND st_r.[LAST_MODIFIED] > GETDATE () - 365 
            AND st_r.[LAST_MODIFIED] < CAST(CAST(GETDATE () - 1 AS DATE) AS DATETIME);
        
        RETURN COALESCE(@EmailMissingRecipientCount, -1);  
    END TRY
    
    BEGIN CATCH
        DECLARE 
             @@MESSAGE      VARCHAR(2048)
            ,@@PROCNAME     VARCHAR(255) 
            ,@@USERNAME     VARCHAR(32)
    
            ,@ErrorNumber   INT 
            ,@ErrorSeverity INT 
            ,@ErrorState    INT 
            ,@ErrorLine     INT 
            ,@ErrorMessage  VARCHAR(1000) 
     
        SELECT   
               @ErrorNumber  = ERROR_NUMBER() 
              ,@ErrorSeverity= ERROR_SEVERITY() 
              ,@ErrorState   = ERROR_STATE() 
              ,@ErrorLine    = ERROR_LINE() 
              ,@ErrorMessage = COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
    
        SELECT  @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')  
        SELECT  @@USERNAME = USER_NAME();  
        SELECT  @@MESSAGE = 'Procedure '       + @@PROCNAME 
                          + ' executed by '    + @@USERNAME + ' failed with error number: ' +CAST(@ErrorNumber AS VARCHAR)
                          +', severity: '      + CAST(@ErrorSeverity AS VARCHAR) 
                          +', state: '         + CAST(@ErrorState AS VARCHAR)
                          +', on line: '       + CAST(@ErrorLine AS VARCHAR)
                          +', with message: '  + @ErrorMessage;    
        RAISERROR (@@MESSAGE 
                  ,@ErrorSeverity 
                   -- Optionally override the error to cause a failure 
                   --,18 
                  ,@ErrorState
                  );
        RETURN -1
    END CATCH
END
GO
