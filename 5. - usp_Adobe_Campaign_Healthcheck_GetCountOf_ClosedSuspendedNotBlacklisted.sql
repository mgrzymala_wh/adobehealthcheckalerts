USE [DMOperations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        WHGROUP\dskowronski
-- Create date:   30/10/2020
-- Description:   DANSDPSRE-221 - Create usp_Adobe_Campaign_Healthcheck_GetCountOf_ClosedSuspendedNotBlacklisted
--                Suppression Rule 2 count of all customer accounts that are closed or suspended and
--                are not blacklisted. If this count fails it means UNO BSL contains customer accounts
--                that are closed or suspended but have not been blacklisted and must be investigated
--                immediately.

-- List of abbreviations:
-- cadp = BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile
-- cap = BusinessSemanticLayer.dbo.CustomerAccountProfile
-- cas = BusinessSemanticLayer.dbo.CustomerAccountStatus
-- =============================================
-- =============================================
-- Change History
-- Date                 User                     Change                
-- 30/10/2020           dskowronski              Created DANSDPSRE-221
-- 5/11/2020            mgrzymala                Fixed a bug: z.CustomerAccountStatusCd IN instead of: z.CustomerAccountStatusNm IN 
-- =============================================

CREATE OR ALTER PROCEDURE dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_ClosedSuspendedNotBlacklisted]
AS
BEGIN
    DECLARE @Adobe_Campaign_CountOf_ClosedSuspendedNotBlacklisted INT;
    BEGIN TRY

      SELECT
          @Adobe_Campaign_CountOf_ClosedSuspendedNotBlacklisted = COUNT(1)
      FROM
          BusinessSemanticLayer.dbo.CustomerAccount ca (nolock) 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile cadp (nolock) 
              ON ca.customeraccountid = cadp.CustomerAccountId 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountProfile cap (nolock) 
              ON ca.CustomerAccountId = cap.CustomerAccountId 
          INNER JOIN
              BusinessSemanticLayer.dbo.CustomerAccountStatus cas (nolock) 
              ON cap.CustomerAccountStatusId = cas.CustomerAccountStatusId 
      WHERE
          --z.CustomerAccountStatusNm IN -- <== 
          cas.CustomerAccountStatusCd IN 
          (
              'C',
              'S' 
          )
          AND ca.CustomerAccountId > 3 AND cadp.BlacklistFlag = 'N'

      RETURN COALESCE(@Adobe_Campaign_CountOf_ClosedSuspendedNotBlacklisted, -1);
    END TRY
    
    BEGIN CATCH
        DECLARE 
             @@MESSAGE      VARCHAR(2048)
            ,@@PROCNAME     VARCHAR(255) 
            ,@@USERNAME     VARCHAR(32)
    
            ,@ErrorNumber   INT 
            ,@ErrorSeverity INT 
            ,@ErrorState    INT 
            ,@ErrorLine     INT 
            ,@ErrorMessage  VARCHAR(1000) 
     
        SELECT   
               @ErrorNumber  = ERROR_NUMBER() 
              ,@ErrorSeverity= ERROR_SEVERITY() 
              ,@ErrorState   = ERROR_STATE() 
              ,@ErrorLine    = ERROR_LINE() 
              ,@ErrorMessage = COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
    
        SELECT  @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')  
        SELECT  @@USERNAME = USER_NAME();  
        SELECT  @@MESSAGE = 'Procedure '       + @@PROCNAME 
                          + ' executed by '    + @@USERNAME + ' failed with error number: ' +CAST(@ErrorNumber AS VARCHAR)
                          +', severity: '      + CAST(@ErrorSeverity AS VARCHAR) 
                          +', state: '         + CAST(@ErrorState AS VARCHAR)
                          +', on line: '       + CAST(@ErrorLine AS VARCHAR)
                          +', with message: '  + @ErrorMessage;    
        RAISERROR (@@MESSAGE 
                  ,@ErrorSeverity 
                   -- Optionally override the error to cause a failure 
                   --,18 
                  ,@ErrorState
                  );
        RETURN -1
    END CATCH
END
GO
