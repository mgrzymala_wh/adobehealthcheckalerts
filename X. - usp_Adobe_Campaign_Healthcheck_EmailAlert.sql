USE [DMOperations]
GO

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
  
-- =============================================  
-- Author:  WHGROUP\mgrzymala  
-- Create date: 30/10/2020  
-- Description: Email notification for Adobe Healthcheck Status
-- =============================================  
-- Change History  
-- Date                 User            Change    
-- 30/10/2020           mgrzymala       created  DANSDPSRE-221 - Create usp_Adobe_Campaign_Healthcheck
-- 07/07/2021           mgrzymala       DANSDPSRE-868 - change logic of the 1st Health Check - removing join to [StagingAdobeInbound].dbo.recipients and linking Staging-BSL directly by RecipientId
-- =============================================  
DROP PROCEDURE IF EXISTS [dbo].[usp_Adobe_Campaign_Healthcheck_EmailAlert]   
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_Adobe_Campaign_Healthcheck_EmailAlert]  
AS  
BEGIN          
BEGIN TRY  
SET NOCOUNT ON;
SET XACT_ABORT ON;

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE   @CheckCondition NVARCHAR(256)
        , @Result INT
        , @DbmailProfile VARCHAR(MAX)   = 'DBA' -- 'DPE_DEV' --  
        , @AlertRecipients VARCHAR(MAX) = 'fernando.lago@williamhill.com; Alex.Manea@williamhill.com; Marcin.Bukowski@grandparade.co.uk; DMOperationalSupport@williamhill.co.uk'  
        , @InfoRecipients VARCHAR(MAX)  = 'fernando.lago@williamhill.com; Alex.Manea@williamhill.com; Marcin.Bukowski@grandparade.co.uk; DMOperationalSupport@williamhill.co.uk'  
        , @CopyRecipients VARCHAR(MAX)  = NULL
        , @PageDutyEmailAddress VARCHAR(256) = 'prd_uno_package-failures@williamhill.pagerduty.com' -- ATTENTION!!!: when testing replace with your email address (for example: 'marek.grzymala@grandparade.co.uk') otherwise On-call person will get a phone-call             , @CheckCondition NVARCHAR(256)  
        , @MailMessage VARCHAR(MAX)  

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- temp table definition:
DROP TABLE IF EXISTS #AdobeRowCounts;
CREATE TABLE #AdobeRowCounts(
             [CheckNumber] INT IDENTITY(1,1) NOT NULL,
	         [CheckCondition] VARCHAR(256)
            ,[DbName] VARCHAR(255)
	        ,[TableName] VARCHAR(255)
	        ,[Result] INT NULL
);

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Check #1 - Distinct count of RECIPIENT_IDs that did not get transfered from [StagingAdobeInbound].dbo.unsubscribe_history to [BusinessSemanticLayer].dbo.UnsubscribeHistory':
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT @CheckCondition = 'Distinct count of RECIPIENT_IDs that did not get transfered from [StagingAdobeInbound].dbo.unsubscribe_history to [BusinessSemanticLayer].dbo.UnsubscribeHistory';
SELECT @Result = COUNT(DISTINCT uh.RECIPIENT_ID)
FROM             [StagingAdobeInbound].dbo.unsubscribe_history uh
--INNER JOIN       [StagingAdobeInbound].dbo.recipients r ON uh.RECIPIENT_ID = r.RECIPIENT_ID
--LEFT  JOIN       [BusinessSemanticLayer].dbo.UnsubscribeHistory uhBSL ON uhBSL.CustomerAccountId = r.GLOBAL_ACCOUNT_ID
LEFT  JOIN    [BusinessSemanticLayer].dbo.UnsubscribeHistory uhBSL ON uhBSL.RecipientId = uh.RECIPIENT_ID
WHERE            
                 uhBSL.CustomerAccountId IS NULL --> find CustomerAccountIds that are in Staging unsubscribe_history but not in BSL UnsubscribeHistory 
AND              uh.RECIPIENT_ID NOT IN (SELECT RECIPIENT_ID FROM StagingAdobeInbound.dbo.recipients WHERE GLOBAL_ACCOUNT_ID = 0) --> exclude GLOBAL_ACCOUNT_ID = 0 (they don't get transfered to BSL)

INSERT INTO      #AdobeRowCounts
SELECT        
                  @CheckCondition             AS [CheckCondition]
                 ,'[BusinessSemanticLayer]'   AS [DbName]
                 ,'[UnsubscribeHistory]'      AS [TableName]
                 , COALESCE(@Result, -2)      AS [Result]


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Check #2 - Distinct count of RECIPIENT_IDs from [StagingAdobeInbound].[dbo].[recipients] with BSL.[CustomerAccountDerivedProfile].[BlacklistEmailFlag] = 'N':
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT @CheckCondition = 'Distinct count of RECIPIENT_IDs from [StagingAdobeInbound].[dbo].[recipients] with BSL CustomerAccountDerivedProfile BlackListFlag set to Y but [BlacklistEmailFlag] = ''N'''
EXEC @Result = dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_Missing_Email_Recipients]

INSERT INTO      #AdobeRowCounts
SELECT        
                  @CheckCondition             AS [CheckCondition]
                 ,'[StagingAdobeInbound]'     AS [DbName]
                 ,'[recipients]'              AS [TableName]
                 , COALESCE(@Result, -2)      AS [Result]

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Check #3 - Distinct count of RECIPIENT_IDs from [StagingAdobeInbound].[dbo].[recipients] with BSL.[CustomerAccountDerivedProfile].[BlacklistMobileFlag] = 'N':
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT @CheckCondition = 'Distinct count of RECIPIENT_IDs from [StagingAdobeInbound].[dbo].[recipients] with BSL CustomerAccountDerivedProfile BlackListFlag set to Y but [BlacklistMobileFlag] = ''N'''
EXEC @Result = dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_Missing_SMS_Recipients]

INSERT INTO      #AdobeRowCounts
SELECT        
                  @CheckCondition             AS [CheckCondition]
                 ,'[StagingAdobeInbound]'     AS [DbName]
                 ,'[recipients]'              AS [TableName]
                 , COALESCE(@Result, -2)      AS [Result]

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Check #4 - Count of all Customer Accounts that have failed Age Verification and are not blacklisted. 
-- IF this count fails it means UNO BSL contains customers who have failed AV but have not been blacklisted and must be investigated immediately.:
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT @CheckCondition = 'Count of all Customer Accounts that have failed Age Verification and are not blacklisted'
EXEC @Result = dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_FailedAgeNotBlacklisted]

INSERT INTO      #AdobeRowCounts
SELECT        
                  @CheckCondition                   AS [CheckCondition]
                 ,'[BusinessSemanticLayer]'         AS [DbName]
                 ,'[CustomerAccountFlagProfile]'    AS [TableName]
                 , COALESCE(@Result, -2)            AS [Result]


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Check #5 - Count of all customer accounts that are closed or suspended and are not blacklisted. 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT @CheckCondition = 'Count of all customer accounts that are closed or suspended and are not blacklisted'
EXEC @Result = dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_ClosedSuspendedNotBlacklisted]

INSERT INTO  #AdobeRowCounts
SELECT       
              @CheckCondition             AS [CheckCondition]
             ,'[BusinessSemanticLayer]'   AS [DbName]
             ,'[CustomerAccountStatus]'   AS [TableName]
             , COALESCE(@Result, -2)      AS [Result]

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Check #6 - Count of all customer accounts that have not registered via Spanish or Italian channels and have an address country 
-- which is a banned country and are not blacklisted. 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT @CheckCondition = 'Count of all customer accounts that have not registered via Spanish or Italian channels and have an address country 
which is a banned country and are not blacklisted'
EXEC @Result = dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_BannedAddressNotBlacklisted]

INSERT INTO  #AdobeRowCounts
SELECT       
              @CheckCondition             AS [CheckCondition]
             ,'[BusinessSemanticLayer]'   AS [DbName]
             ,'[Channel]'                 AS [TableName]
             , COALESCE(@Result, -2)      AS [Result]

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Check #7 - Count of all customer accounts that are flagged as test account and are not blacklisted. 
-- If this count fails it means UNO BSL contains customers who have been flagged as test accounts but have not been blacklisted and must be investigated immediately:
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT @CheckCondition = 'Count of all customer accounts that are flagged as test account and are not blacklisted
If this count fails it means UNO BSL contains customers who have been flagged as test accounts but have not been blacklisted and must be investigated immediately'
EXEC @Result = dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_FlaggedAsTestAccountNotBlacklisted]

INSERT INTO  #AdobeRowCounts
SELECT       
              @CheckCondition                   AS [CheckCondition]
             ,'[BusinessSemanticLayer]'         AS [DbName]
             ,'[CustomerAccountFlagProfile]'    AS [TableName]
             , COALESCE(@Result, -2)            AS [Result]

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Check #8 - Count of all customer accounts that have a flag set that should mean they are blacklisted but are not blacklisted. 
-- If this count fails it means UNO BSL contains customers who have certain flags set that should mean that they are blacklisted 
-- but they have not been blacklisted and must be investigated immediately.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT @CheckCondition = 'Count of all customer accounts that have a flag set that should mean they are blacklisted but are not blacklisted. If this count fails it means UNO BSL 
contains customers who have certain flags set that should mean that they are blacklisted but they have not been blacklisted and must be investigated immediately'
EXEC @Result = dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_FlagSetNotBlacklisted]

INSERT INTO  #AdobeRowCounts
SELECT       
              @CheckCondition                   AS [CheckCondition]
             ,'[BusinessSemanticLayer]'         AS [DbName]
             ,'[CustomerAccountFlagProfile]'    AS [TableName]
             , COALESCE(@Result, -2)            AS [Result]

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- if any of the check conditions return Results <> 0 send an alert to @PageDutyEmailAddress:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT Result FROM #AdobeRowCounts WHERE (Result < 0 OR Result > 5)) -- send and SMS alert only if any counts are negative (an error) or > 5
BEGIN  
     SELECT @CopyRecipients = @PageDutyEmailAddress  
END  
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- build CSS header and HTML doc:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @CSS XML
DECLARE @ColumnWidth NVARCHAR(32) = '55%'
SELECT @CSS = dbo.ufn_CreateHTMLCSS
(
    @ColumnWidth
)

DECLARE @XmlTable XML
SELECT @XmlTable = dbo.ufn_CreateHTMLTable
(
    (       
        SELECT 
                [CheckNumber],
                [CheckCondition],
                [DbName],
                [TableName],
                CASE --Result 
                     WHEN Result < 0 THEN CONCAT('<div class="Critical">', CAST(Result AS NVARCHAR(32)), '</div>') 
                     WHEN Result = 0 THEN CONCAT('<div class="Healthy">', CAST(Result AS NVARCHAR(32)), '</div>') 
                     WHEN Result > 0 AND Result <= 5 THEN CONCAT('<div class="Warning">', CAST(Result AS NVARCHAR(32)), '</div>')
                     WHEN Result > 5 THEN CONCAT('<div class="Critical">', CAST(Result AS NVARCHAR(32)), '</div>') 
                     ELSE CONCAT('<div class="Critical">', CAST(Result AS NVARCHAR(32)), '</div>') 
                END AS [Result]      
        FROM   #AdobeRowCounts 
        FOR XML PATH('row'), ELEMENTS XSINIL, TYPE
    ),  't1', 't2', 't3'
);

------------------------------------------------------------------------------
 -- this is just to replace "&lt;" and "&gt;" with "<" and ">" inside the xml:
DECLARE @HtmlOutput NVARCHAR(MAX)
SET @HtmlOutput = CONVERT(NVARCHAR(MAX), @XmlTable)
SET @HtmlOutput = REPLACE(REPLACE(@HtmlOutput, '&lt;', '<'), '&gt;', '>')
------------------------------------------------------------------------------
SET @HtmlOutput = '<h3>This is an automated email alert from server: '+CONVERT(NVARCHAR(256), @@SERVERNAME)+' - Please do not reply.</h3>
                   <table><h4>Status of Adobe Healthchecks:</h4></table>'+@HtmlOutput

SELECT @MailMessage = (SELECT @CSS, CAST(@HtmlOutput AS XML) FOR XML PATH('html'))
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- send alert email:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
EXEC msdb.dbo.sp_send_dbmail   
              @profile_name = @DbmailProfile,  
              @recipients = @AlertRecipients,  
              @copy_recipients = @CopyRecipients,  
              @subject = 'Adobe Dataload Healthchecks Status',  
              @body = @MailMessage,  
              @body_format = 'HTML';
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
END TRY   
BEGIN CATCH   
	DECLARE
		 @@MESSAGE VARCHAR(2048)
        ,@@PROCNAME VARCHAR(255) 
        ,@@USERNAME VARCHAR(32)

		,@ErrorNumber	INT 
		,@ErrorSeverity	INT 
		,@ErrorState	INT 
		,@ErrorLine		INT 
		,@ErrorMessage	VARCHAR(1000) 
 
    SELECT   
  		 @ErrorNumber	    = ERROR_NUMBER() 
  		,@ErrorSeverity     = ERROR_SEVERITY() 
  	    ,@ErrorState	    = ERROR_STATE() 
  	    ,@ErrorLine		    = ERROR_LINE() 
  	    ,@ErrorMessage	    = COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)

    SELECT  @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')  
    SELECT  @@USERNAME = USER_NAME();  
    SELECT  @@MESSAGE = 'Procedure ' + @@PROCNAME + ' executed by '  + @@USERNAME + ' failed with error number: ' +CAST(@ErrorNumber AS VARCHAR)
                                         +', severity: '      +CAST(@ErrorSeverity AS VARCHAR) 
                                         +', state: '         +CAST(@ErrorState AS VARCHAR)
                                         +', on line: '       +CAST(@ErrorLine AS VARCHAR)
                                         +', with message: '  +@ErrorMessage;    
		        RAISERROR (@@MESSAGE 
		                  ,@ErrorSeverity 
		        		   -- Optionally override the error to cause a failure 
		        		   --,18 
		                  ,@ErrorState
		                  );
    RETURN -1  
END CATCH;   
END;  
GO

