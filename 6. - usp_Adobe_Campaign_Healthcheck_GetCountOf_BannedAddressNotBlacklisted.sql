USE [DMOperations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        WHGROUP\dskowronski
-- Create date:   30/10/2020
-- Description:   DANSDPSRE-221 - Create usp_Adobe_Campaign_Healthcheck_GetCountOf_BannedAddressNotBlacklisted
--                Suppression Rule 3 count of all customer accounts that have not registered
--                via Spanish or Italian channels and have an address country which is a banned 
--                country and are not blacklisted. If this count fails it must be investigated
--                immediately.

-- List of abbreviations:
-- cadp = BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile
-- cafp = BusinessSemanticLayer.dbo.CustomerAccountFlagProfile
-- cap = BusinessSemanticLayer.dbo.CustomerAccountProfile
-- cas = BusinessSemanticLayer.dbo.CustomerAccountStatus
-- ca = BusinessSemanticLayer.dbo.CustomerAccount
-- cnt = BusinessSemanticLayer.dbo.Country
-- =============================================
-- =============================================
-- Change History
-- Date                 User                     Change                
-- 30/10/2020           dskowronski              Created DANSDPSRE-221
-- =============================================

DROP PROCEDURE IF EXISTS dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_BannedAddressNotBlacklisted]
GO

CREATE OR ALTER PROCEDURE dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_BannedAddressNotBlacklisted]
AS
BEGIN
    DECLARE @usp_Adobe_Campaign_CountOf_BannedAddressNotBlacklisted INT;
    BEGIN TRY

            SELECT      @usp_Adobe_Campaign_CountOf_BannedAddressNotBlacklisted = COUNT(DISTINCT ca.CustomerAccountId)
            FROM        BusinessSemanticLayer.dbo.Address a (NOLOCK)
            INNER JOIN  BusinessSemanticLayer.dbo.CustomerAccount ca (NOLOCK)                   ON ca.CustomerAccountId = a.CustomerAccountId
            INNER JOIN  BusinessSemanticLayer.dbo.Country cnt (NOLOCK)                          ON ca.RegistrationCountryId = cnt.CountryId
            INNER JOIN  BusinessSemanticLayer.dbo.CustomerAccountDerivedProfile cadp (NOLOCK)   ON ca.customeraccountid = cadp.customeraccountid
            INNER JOIN  BusinessSemanticLayer.dbo.CustomerAccountFlagProfile cafp (NOLOCK)      ON ca.customeraccountid = cafp.customeraccountid
            INNER JOIN  BusinessSemanticLayer.dbo.CustomerAccountProfile cap (NOLOCK)           ON ca.customeraccountid = cap.customeraccountid
            INNER JOIN  BusinessSemanticLayer.dbo.CustomerAccountStatus cas (NOLOCK)            ON cap.CustomerAccountStatusId = cas.CustomerAccountStatusId
            INNER JOIN  BusinessSemanticLayer.dbo.Channel ch (NOLOCK)                           ON ch.ChannelId = ca.registrationchannelid
            WHERE       ch.ChannelCd NOT IN ('A','E','V','U')
            AND         (cnt.CanRegisterIndicator = 'N' 
            OR          cnt.CanDepositIndicator = 'N' 
            OR          cnt.CanExternalTransferIndicator = 'N' )
            AND         cadp.blacklistflag = 'N'
            AND         ca.CustomerAccountId > 3 

      RETURN COALESCE(@usp_Adobe_Campaign_CountOf_BannedAddressNotBlacklisted, -1);
    END TRY
    
    BEGIN CATCH
        DECLARE 
             @@MESSAGE      VARCHAR(2048)
            ,@@PROCNAME     VARCHAR(255) 
            ,@@USERNAME     VARCHAR(32)
    
            ,@ErrorNumber   INT 
            ,@ErrorSeverity INT 
            ,@ErrorState    INT 
            ,@ErrorLine     INT 
            ,@ErrorMessage  VARCHAR(1000) 
     
        SELECT   
               @ErrorNumber  = ERROR_NUMBER() 
              ,@ErrorSeverity= ERROR_SEVERITY() 
              ,@ErrorState   = ERROR_STATE() 
              ,@ErrorLine    = ERROR_LINE() 
              ,@ErrorMessage = COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
    
        SELECT  @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')  
        SELECT  @@USERNAME = USER_NAME();  
        SELECT  @@MESSAGE = 'Procedure '       + @@PROCNAME 
                          + ' executed by '    + @@USERNAME + ' failed with error number: ' +CAST(@ErrorNumber AS VARCHAR)
                          +', severity: '      + CAST(@ErrorSeverity AS VARCHAR) 
                          +', state: '         + CAST(@ErrorState AS VARCHAR)
                          +', on line: '       + CAST(@ErrorLine AS VARCHAR)
                          +', with message: '  + @ErrorMessage;    
        RAISERROR (@@MESSAGE 
                  ,@ErrorSeverity 
                   -- Optionally override the error to cause a failure 
                   --,18 
                  ,@ErrorState
                  );
        RETURN -1
    END CATCH
END
GO
