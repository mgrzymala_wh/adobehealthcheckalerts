USE [DMOperations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        WHGROUP\dskowronski
-- Create date:   29/10/2020
-- Description:   DANSDPSRE-221 - Create [usp_Adobe_Campaign_Healthcheck_GetCountOf_Missing_SMS_Recipients]
--                Returns distinct count of RECIPIENT_IDs from [StagingAdobeInbound].[dbo].[recipients] with BSL.[CustomerAccountDerivedProfile].[BlacklistSMSFlag] = 'N'  

-- List of abbreviations:

-- bsl_cadp     = [BusinessSemanticLayer].[dbo].[CustomerAccountDerivedProfile] 
-- pl_cab       = [PresentationLayer].[dbo].[CustomerAccountBase]
-- st_pf        = [StagingOpenbetOutbound].[dbo].[Preferenceflags]
-- st_pf_max    = SELECT [cust_id], MAX([RequestId]) req FROM [StagingOpenbetOutbound].[dbo].[Preferenceflags] GROUP BY [cust_id]
-- st_pfes      = [StagingOpenbetOutbound].[dbo].[PreferenceflagsES]
-- st_pfes_max  = SELECT [cust_id], MAX([RequestId]) req FROM [StagingOpenbetOutbound].[dbo].[PreferenceflagsES]  GROUP BY[cust_id]
-- st_pfit      = [StagingOpenbetOutbound].[dbo].[PreferenceflagsIT]  
-- st_pfit_max  = SELECT [cust_id], MAX([RequestId]) req FROM[StagingOpenbetOutbound].[dbo].[PreferenceflagsIT] GROUP BY [cust_id]
-- st_pf_multi  = unioned quieries on st_pf_max, st_pfit_max and st_pfes_max
-- st_r         = [StagingAdobeInbound].[dbo].[recipients] 
-- st_rd        = SELECT DISTINCT RECIPIENT_ID, GLOBAL_ACCOUNT_ID FROM [StagingAdobeInbound].[dbo].[recipients]
-- st_rmax      = SELECT [GLOBAL_ACCOUNT_ID],MAX([RequestId]) req FROM [StagingAdobeInbound].[dbo].[recipients] GROUP BY[GLOBAL_ACCOUNT_ID]
-- st_tc        = [StagingOpenbet].[dbo].[tcustomerreg] 
-- st_uh        = [StagingAdobeInbound].[dbo].[Unsubscribe_History]
-- st_uh_joined = st_uh joined with pl_cab and st_r
-- st_uh_final  = st_uh_joined with more joins and wheres

-- =============================================
-- =============================================
-- Change History
-- Date                 User                     Change                
-- 29/10/2020           dskowronski              Created DANSDPSRE-221
-- 02/11/2020           dskowronski              abbreviations
-- 05/11/2020           mgrzymala                moved DISTINCT after COUNT(DISTINCT
-- =============================================
DROP PROCEDURE IF EXISTS dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_Missing_SMS_Recipients]
GO

CREATE OR ALTER PROCEDURE dbo.[usp_Adobe_Campaign_Healthcheck_GetCountOf_Missing_SMS_Recipients]
AS
BEGIN
    DECLARE @SMSMissingRecipientCount INT;

    BEGIN TRY
        SELECT 
            @SMSMissingRecipientCount = COUNT(DISTINCT st_r.[RECIPIENT_ID]) 
        FROM
            [StagingAdobeInbound].[dbo].[recipients] AS st_r 
            INNER JOIN
                (
                    SELECT
                        [GLOBAL_ACCOUNT_ID],
                        MAX([RequestId]) req 
                    FROM
                        [StagingAdobeInbound].[dbo].[recipients] 
                    GROUP BY
                        [GLOBAL_ACCOUNT_ID]
                )
                AS st_rmax 
                ON st_r.GLOBAL_ACCOUNT_ID = st_rmax.GLOBAL_ACCOUNT_ID 
                AND st_r.RequestId = st_rmax.req 
            INNER JOIN
                [BusinessSemanticLayer].[dbo].[CustomerAccountDerivedProfile] AS bsl_cadp
                ON bsl_cadp.CustomerAccountId = st_r.[GLOBAL_ACCOUNT_ID] 
            INNER JOIN
                [PresentationLayer].[dbo].[CustomerAccountBase] AS pl_cab
                ON bsl_cadp.CustomerAccountId = pl_cab.CustomerAccountId 
            LEFT OUTER JOIN
                (
                    SELECT
                        st_pf.[cust_id],
                        [Acct_no],
                        [contact_how],
                        [contact_ok],
                        [RequestId],
                        [SourceSystemId] 
                    FROM
                        [StagingOpenbetOutbound].[dbo].[Preferenceflags] AS st_pf
                        INNER JOIN
                            (
                                SELECT
                                    [cust_id],
                                    MAX([RequestId]) req 
                                FROM
                                    [StagingOpenbetOutbound].[dbo].[Preferenceflags] 
                                GROUP BY
                                    [cust_id]
                            )
                            AS st_pf_max
                            ON st_pf.cust_id = st_pf_max.cust_id 
                            AND st_pf.RequestId = st_pf_max.req 
                        UNION
                        SELECT
                            st_pfes.[cust_id],
                            [Acct_no],
                            [contact_how],
                            [contact_ok],
                            [RequestId],
                            [SourceSystemId] 
                        FROM
                            [StagingOpenbetOutbound].[dbo].[PreferenceflagsES] AS st_pfes
                            INNER JOIN
                                (
                                    SELECT
                                        [cust_id],
                                        MAX([RequestId]) req 
                                    FROM
                                        [StagingOpenbetOutbound].[dbo].[PreferenceflagsES] 
                                    GROUP BY
                                        [cust_id]
                                )
                                AS st_pfes_max 
                                ON st_pfes.cust_id = st_pfes_max.cust_id 
                                AND st_pfes.RequestId = st_pfes_max.req 
                            UNION
                            SELECT
                                st_pfit.[cust_id],
                                [Acct_no],
                                [contact_how],
                                [contact_ok],
                                [RequestId],
                                [SourceSystemId] 
                            FROM
                                [StagingOpenbetOutbound].[dbo].[PreferenceflagsIT] AS st_pfit 
                                INNER JOIN
                                    (
                                        SELECT
                                            [cust_id],
                                            MAX([RequestId]) req 
                                        FROM
                                            [StagingOpenbetOutbound].[dbo].[PreferenceflagsIT] 
                                        GROUP BY
                                            [cust_id]
                                    )
                                    AS st_pfit_max 
                                    ON st_pfit.cust_id = st_pfit_max.cust_id 
                                    AND st_pfit.RequestId = st_pfit_max.req 
                )
                AS st_pf_multi
                ON st_pf_multi.[Acct_no] = pl_cab.CustomerAccountNumber 
            LEFT OUTER JOIN
                [StagingOpenbet].[dbo].[tcustomerreg] AS st_tc
                ON pl_cab.CustomerReferenceNumber = st_tc.cust_id 
            LEFT OUTER JOIN
                (
                    SELECT
                        st_uh.[RECIPIENT_ID],
                        pl_cab.[CustomerAccountId],
                        st_uh.[RECIPIENT_EMAIL_ADDRESS],
                        st_uh.[SOURCE_URL],
                        st_uh.[CREATED] AS [CreatedDt],
                        st_uh.[Message] 
                    FROM
                        [StagingAdobeInbound].[dbo].[Unsubscribe_History] AS st_uh
                        INNER JOIN
                            (
                                SELECT DISTINCT
                                    RECIPIENT_ID,
                                    GLOBAL_ACCOUNT_ID 
                                FROM
                                    [StagingAdobeInbound].[dbo].[recipients]
                            )
                            AS st_rd
                            ON st_uh.RECIPIENT_ID = st_rd.RECIPIENT_ID 
                        INNER JOIN
                            [PresentationLayer].[dbo].[CustomerAccountBase] AS pl_cab
                            ON st_rd.GLOBAL_ACCOUNT_ID = pl_cab.CustomerAccountId 
                        INNER JOIN
                            (
                                SELECT
                                    [CustomerAccountId],
                                    MAX(st_uh.requestid) reqid 
                                FROM
                                    [StagingAdobeInbound].[dbo].[Unsubscribe_History] AS st_uh 
                                    INNER JOIN
                                        [StagingAdobeInbound].[dbo].[recipients] AS st_r 
                                        ON st_uh.RECIPIENT_ID = st_r.RECIPIENT_ID 
                                    INNER JOIN
                                        [PresentationLayer].[dbo].[CustomerAccountBase] AS pl_cab 
                                        ON st_r.GLOBAL_ACCOUNT_ID = pl_cab.CustomerAccountId 
                                WHERE
                                    [Message] IS NOT NULL 
                                GROUP BY
                                    [CustomerAccountId]
                            )
                            AS st_uh_joined
                            ON st_uh.RequestId = st_uh_joined.reqid 
                            AND pl_cab.CustomerAccountId = st_uh_joined.CustomerAccountId 
                    WHERE
                        st_uh.[Message] IS NOT NULL 
                        AND st_uh.[CREATED] > '2020-09-13' 
                )
                AS st_uh_final 
                ON bsl_cadp.CustomerAccountId = st_uh_final.CustomerAccountId 
        WHERE
            1 = 1 
            AND bsl_cadp.[BlacklistFlag] <> 'Y' 
            AND bsl_cadp.[BlacklistMobileFlag] = 'N' 
            AND st_r.[LAST_MODIFIED] > getdate () - 365 
            AND st_r.[LAST_MODIFIED] < CAST(CAST(GETDATE () - 1 AS DATE) AS DATETIME) 
            AND st_uh_final.[Message] IS NOT NULL;

        RETURN COALESCE(@SMSMissingRecipientCount, -1);
    END TRY
    
    BEGIN CATCH
        DECLARE 
             @@MESSAGE      VARCHAR(2048)
            ,@@PROCNAME     VARCHAR(255) 
            ,@@USERNAME     VARCHAR(32)
    
            ,@ErrorNumber   INT 
            ,@ErrorSeverity INT 
            ,@ErrorState    INT 
            ,@ErrorLine     INT 
            ,@ErrorMessage  VARCHAR(1000) 
     
        SELECT   
               @ErrorNumber  = ERROR_NUMBER() 
              ,@ErrorSeverity= ERROR_SEVERITY() 
              ,@ErrorState   = ERROR_STATE() 
              ,@ErrorLine    = ERROR_LINE() 
              ,@ErrorMessage = COALESCE(ERROR_MESSAGE(), 'Unknown') + CHAR(13)
    
        SELECT  @@PROCNAME = ISNULL(OBJECT_NAME(@@PROCID), 'Unknown Procedure')  
        SELECT  @@USERNAME = USER_NAME();  
        SELECT  @@MESSAGE = 'Procedure '       + @@PROCNAME 
                          + ' executed by '    + @@USERNAME + ' failed with error number: ' +CAST(@ErrorNumber AS VARCHAR)
                          +', severity: '      + CAST(@ErrorSeverity AS VARCHAR) 
                          +', state: '         + CAST(@ErrorState AS VARCHAR)
                          +', on line: '       + CAST(@ErrorLine AS VARCHAR)
                          +', with message: '  + @ErrorMessage;    
        RAISERROR (@@MESSAGE 
                  ,@ErrorSeverity 
                   -- Optionally override the error to cause a failure 
                   --,18 
                  ,@ErrorState
                  );
        RETURN -1
    END CATCH
END
GO
